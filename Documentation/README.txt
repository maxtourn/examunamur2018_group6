                     _________
                    |         |
                    | READ ME |
                    |_________|



PROJECT TITLE: examunamur2018_group6
-------------

This project makes mathematical analysis on a database from a coffee bar. Also, it extracts the consumption probabilities
of the customers.
It uses these probabilities to create a new database over 5 years with new customers (converted in classes): returning
customers, hipsters customers, one time customers and tripadvisor customers.

CUSTOMERS
---------

ReturningCustomer: regular customer that comes more than once in the coffee bar.
HipsterCustomer: subclasse of ReturningCustomer but has another budget than the returning one.

OneTimeCustomer: regular customer that comes only once to the coffee bar.
TripAdvisorCustomer: subclasse of OneTimeCustomer but gives a tip to the coffee bar.

RUNNING THE SIMULATION
----------------------

The simulation is written in Simulation.py. The standard use and the results are in simulation_and_plots.py.

Standard simulation:

                    Returning Customer      Hipsters     One Time Customer      TripAdvisor Customer
_______________________________________________________________________________________________________________________
number        |       (2/3)*1000           (1/3)*1000      not predefined          not predefined
              |
budget        |           250                  500              100                      100
              |
Probabilities |      (2/10)*(2/3)         (2/10)*(1/3)     (8/10)*(9/10)            (9/10)*(1/10)
______________|________________________________________________________________________________________________________


CREATE HIS OWN SIMULATION
-------------------------

2 functions are used:

- create_returning: function that creates the ReturningCustomers and the HipstersCustomers.
Parameters:
consumption: probabilities of consumption at any time (dict).
nbr_returning=1000: number of returning customer (int).
prob_hipster=1/3: the probability that a hipster customer comes among the returning one (float).
budget_returning=250: the budget of the regular returning customers (int).
budget_hipster=500: the budget of the hipster customer (int).

return: a list of the returning customers

- simulation: function that simulates the data of the coffee bar over 5 years.
Parameters:
- TIME: time of every order (df)
- consumption: probabilities of consumption at any time (dict).
- drinks_prices: the prices of drinks from the coffee bar (dict)
- foods_prices: the prices of the foods from the coffee bar (dict)
- returning_customers: the returning customers, they can be created from the create_returning function (list).
- prob_Returning=.2: the probability that a returning customers comes (float)
- prob_tripadvisor=.1: the probability that a tripadvisor comes (float)
- budget_onetime=100: the budget of the one-time customers (int)
- budget_tripadvisor=100: the budget of the tripadvisor customer (int)

return: a dataframe of the simulation over 5 years with time, customerID, drinks and food columns.

There are some examples in Additional_questions.py

An improved version of the simulation is in Improved_Simulation.py and is used in New_questions.py


