from Code.Part1.Probabilities import probabilities
from Code.Part1.Graph import show_graph
from Code.Part2 import Customers
from Code.Part3.Simulation import create_returning
from Code.Part4.Improved_Simulation import simulation
import pandas as pd
from numpy import nan

data = pd.read_csv("../../Data/Coffeebar_2013-2017.csv", sep=";")
drinks_prices = {"milkshake": 5, "frappucino": 4, "water": 2, "coffee": 3, "soda": 3, "tea": 3}
foods_prices = {"sandwich": 5, "cookie": 2, "pie": 3, "muffin": 3, nan: 0}
consumption = probabilities(data)

# Insert at least one one thing you would like to see the impact of


# New question: 50% of the returning customers are hipsters, the probability to have a Returning customer is 25% and
# their number increases of 500
print("Simulation: 50% Hipsters, 25% Returning, 1500 returning")

# Reference simulation
returning_customers = create_returning(consumption)
simulated_data = simulation(data["TIME"], consumption, drinks_prices, foods_prices, returning_customers)

# Tested simulation
returning_customers1 = create_returning(consumption, nbr_returning=1500, prob_hipster=1/2)
simulated_data1 = simulation(data["TIME"], consumption, drinks_prices, foods_prices, returning_customers1,
                             prob_returning=.25)

# graph: Part of the money of the returning customers spend in the reference simulation compared to the tested
# simulation

row_list = []

for list in [returning_customers, returning_customers1]:
    for customer in list:
        if type(customer) == Customers.HipsterCustomer:
            spend = 500 - customer.budget
            kind = "Hipster"
        else:
            spend = 250 - customer.budget
            kind = "Returning"
        dict = {"TYPE": kind, "AMOUNT SPENT": spend}
        row_list.append(dict)

df1 = pd.DataFrame(row_list[:1000], columns=['TYPE', 'AMOUNT SPENT'])  # Reference simulation
df1 = df1.groupby("TYPE").mean()
df2 = pd.DataFrame(row_list[1000:], columns=['TYPE', 'AMOUNT SPENT'])  # Tested simulation
df2 = df2.groupby("TYPE").mean()

df = pd.concat([df1, df2], axis=1)
df.columns = ["Reference simulation", "Tested simulation"]

show_graph(df, 'The average money spending for one returning customer over 5 years')


# ----------------------------------------------------------------------------------------------------------------------

# New question : Create a more realistic simulation
# Now customers can come alone, in group or don't come. There are 3 types of groups: gtoups with only returning
# customers, groups with only onetime customers (without tripadvisor customers) and groups with only tripadvisor
# customers.

# Make a simulation in which the maximum size of groups is 5 for returning, 3 for the onetime and 4 for the tripadvisor.
# For the returning, the probability not to come is 20%, to be alone is null, to be 2 is 10%, to be 3 is 20%, to be 4 is
# 30% and to be 5 is 20%
# For the onetime, the probability not to come is 20%, to be alone is 0.3, to be 2 is 30% and to be 3 is 20%.
# For the tripadvisor, the probability not to come is 20%, to be alone is 0.3, to be 2 is 20%, to be 3 is 20% and to be
# 4 is 10%
print("Simulation: in group")
returning_customers = create_returning(consumption, nbr_returning=2000, budget_hipster=800, budget_returning=400)
best_simulated_data = simulation(data["TIME"], consumption, drinks_prices, foods_prices, returning_customers,
                                 group_size_returning=5, group_size_onetime=3, group_size_tripavisor=4,
                                 group_prob_returning=[.2, 0, .1, .2, .3, .2], group_prob_onetime=[.2, .3, .3, 0.2],
                                 group_prob_tripadvisor=[.2, .3, .2, .2, .1])

# Compare the number of orders placed by returning customers and onetime customers from the above simulation to a
# reference simulation (same parameters as in the simulation of part3)

# Reference simulation
data = pd.read_csv("../../Data/SimulatedData.csv", sep=";")
data2 = data.copy()
data2["TIME"] = data["TIME"].str[11:13]
data2 = data2[["TIME", "CUSTOMER"]].groupby(["CUSTOMER", "TIME"]).size().unstack()

df_returning_reference = data2[data2.sum(1) > 1].sum()
df_onetime_reference = data2[data2.sum(1) == 1].sum()

# Tested simulation
best_simulated_data2 = best_simulated_data.copy()
best_simulated_data2["TIME"] = best_simulated_data["TIME"].str[11:13]
best_simulated_data2 = best_simulated_data2[["TIME", "CUSTOMER"]].groupby(["CUSTOMER", "TIME"]).size().unstack()

df_returning_tested = best_simulated_data2[best_simulated_data2.sum(1) > 1].sum()
df_onetime_tested = best_simulated_data2[best_simulated_data2.sum(1) == 1].sum()

df_comparison = pd.concat([df_returning_reference, df_onetime_reference, df_returning_tested, df_onetime_tested], axis=1)
df_comparison.columns = ["Returning_reference_simulation", "Onetime_reference_simulation",
                         "Returning_tested_simulation", "Onetime_tested_simulation"]

# Graph
show_graph(df_comparison, "Number order comparison between the customers from the reference simulation and the"
                          " improved simulation", annotation=False)
