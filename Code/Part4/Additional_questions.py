from Code.Part1.Probabilities import probabilities
from Code.Part1.Graph import show_graph
from Code.Part3.Simulation import create_returning
from Code.Part3.Simulation import simulation
import pandas as pd
from numpy import nan


data = pd.read_csv("../../Data/Coffeebar_2013-2017.csv", sep=";")
drinks_prices = {"milkshake": 5, "frappucino": 4, "water": 2, "coffee": 3, "soda": 3, "tea": 3}
foods_prices = {"sandwich": 5, "cookie": 2, "pie": 3, "muffin": 3, nan: 0}
consumption = probabilities(data)

# Show some buying histories of returning customers for your simulations
print("'Normal' simulation")
returning_customers = create_returning(consumption)
simulated_data = simulation(data["TIME"], consumption, drinks_prices, foods_prices, returning_customers)

print("Some buying histories of returning customers")
for customer in returning_customers[::123]:  # to give only a few examples of histories
    print("Customer: ", customer.customerID, " History: ", customer.history)

# ----------------------------------------------------------------------------------------------------------------------

# In the provided data set (Coffeebar_2013-2017.csv) there are actual returning customers. How many? Do they have
# specific times when they show up more? Can you determine a probability of having a onetime or returning customer at a
# given time? How does this impact their buying history? Do you see correlations between what returning customers buy
# and one-timers?

# Number of returning customers in the provided data set
CID_returning_customers = data.set_index("CUSTOMER").index.get_duplicates()
print("There are %s returning customers in the provided data set" % len(CID_returning_customers))

# Do they have specific times when they show up more?
data2 = data.copy()
data2["TIME"] = data["TIME"].str[11:13]

df = data2[["TIME", "CUSTOMER"]].groupby(["CUSTOMER", "TIME"]).size().unstack()
df_returning = df[df.sum(1) > 1]  # customers for whom the CID has appeared more than once

returning_show_up = df_returning.sum()/df_returning.sum().sum()

show_graph(returning_show_up, 'Frequency of appearance of the returning customers')

# Probability of having a onetime or returning customer at a given time
prob_returning = df_returning.sum()/df.sum()

df_onetime = df[df.sum(1) == 1]  # customers for whom the CID has appeared once
prob_onetime = df_onetime.sum()/df.sum()

prob = pd.concat([prob_onetime, prob_returning], axis=1)
prob.columns = ['Onetime', 'Returning']

show_graph(prob, 'Probability of having a onetime or a returning customer at any given time')

# How does this impact their buying history?
# Drinks
df_drinks = data2.groupby(["CUSTOMER", "DRINKS"]).size().unstack()

#       returning customers
df_returning_drinks = df_drinks[df_drinks.sum(1) > 1]  # customers for whom the CID has appeared more than once
prob_returning_drinks = df_returning_drinks.sum()/df_returning_drinks.sum().sum()

#       onetime customers
df_onetime_drinks = df_drinks[df_drinks.sum(1) == 1]  # customers for whom the CID has appeared once
prob_onetime_drinks = df_onetime_drinks.sum()/df_onetime_drinks.sum().sum()

#       both in a single dataframe
prob_drinks = pd.concat([prob_onetime_drinks, prob_returning_drinks], axis=1)
prob_drinks.columns = ['Onetime', 'Returning']

show_graph(prob_drinks, 'Distribution of drinks consumption by type of customer')

# Food
df_food = data2.groupby(["CUSTOMER", "FOOD"]).size().unstack()

#       returning customers
df_returning_food = df_food[df_food.sum(1) > 1]
prob_returning_food = df_returning_food.sum()/df_returning_food.sum().sum()

#       onetime customers
df_onetime_food = df_food[df_food.sum(1) == 1]
prob_onetime_food = df_onetime_food.sum()/df_onetime_food.sum().sum()

#       both in a single dataframe
prob_food = pd.concat([prob_onetime_food, prob_returning_food], axis=1)
prob_food.columns = ['Onetime', 'Returning']

show_graph(prob_food, 'Distribution of food consumption by type of customer')

# Do you see correlations between what returning customers buy and one-timers?
# Paper

# ----------------------------------------------------------------------------------------------------------------------

# What would happen if we lower the returning customers to 50 and simulate the same period?
print("Simulation: 50 returning customers")
returning_customers1 = create_returning(consumption, 50)
simulated_data1 = simulation(data["TIME"], consumption, drinks_prices, foods_prices, returning_customers1)
# Returning customers cannot come back before the end of the simulations so there are less orders after the 5 years

# ----------------------------------------------------------------------------------------------------------------------

# The prices change from the beginning of 2015 and go up by 20%
print("Simulation: prices go up by 20% in 2015")
returning_customers2 = create_returning(consumption)

# 2013-2014
simulated_data2_1 = simulation(data.set_index("TIME").loc[: "2014-12-31 17:56:00"].reset_index()["TIME"], consumption,
                               drinks_prices, foods_prices, returning_customers2)

# Increase of prices
drinks_prices_2015 = drinks_prices.copy()
for drink in drinks_prices_2015:
    drinks_prices_2015[drink] *= 1.2

foods_prices_2015 = foods_prices.copy()
for food in foods_prices_2015:
    foods_prices_2015[food] *= 1.2

# 2015-2017
simulated_data2_2 = simulation(data.set_index("TIME").loc["2015-01-01 08:00:00":].reset_index()["TIME"], consumption,
                               drinks_prices_2015, foods_prices_2015, returning_customers2)

# 2013-2017
simulated_data2 = simulated_data2_1.append(simulated_data2_2, ignore_index=True)
simulated_data2.to_csv("../../Data/Simulation_prices_increase_in_2015.csv", sep=";")
# Returning customers cannot come back before the end of the simulations so there are less orders after the 5 years

# ----------------------------------------------------------------------------------------------------------------------

# The budget of hipsters drops to 40
print("Simulation: budget of hipsters drops to 40")
returning_customers3 = create_returning(consumption, budget_hipster=40)
simulated_data3 = simulation(data["TIME"], consumption, drinks_prices, foods_prices, returning_customers3)
simulated_data3.to_csv("../../Data/Simulation_40_budget_hipster.csv", sep=";", index=False)
# Returning customers cannot come back before the end of the simulations so there are less orders after the 5 years
