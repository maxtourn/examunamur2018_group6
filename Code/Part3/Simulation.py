from Code.Part2 import Customers as c
from random import randint, uniform
import pandas as pd


def create_returning(consumption, nbr_returning=1000, prob_hipster=1/3, budget_returning=250, budget_hipster=500):
    """create the fixed number of returning customers and return them as a list"""

    returning_customers = list()  # Complete list of the Returning customers of the bar

    for elt in range(nbr_returning):
        if uniform(0, 1) <= prob_hipster:  # hipster customer
            customer = c.HipsterCustomer("CID" + str(elt + 1), consumption, budget_hipster)
        else:  # regular (returning) customer
            customer = c.ReturningCustomer("CID" + str(elt + 1), consumption, budget_returning)

        returning_customers.append(customer)

    return returning_customers


def simulation(TIME, consumption, drinks_prices, foods_prices, returning_customers, prob_returning=.2,
               prob_tripadvisor=.1, budget_onetime=100, budget_tripadvisor=100):
    """create the simulated data in regard of the probabilities and the number of the different custumers.
       It returns a DataFrame."""

    current_returning_customers = returning_customers.copy()  # Contains only Returning customers who can come back

    row_list = list()  # Contains the simulated data

    for index, time in TIME.items():
        if uniform(0, 1) <= prob_returning:
            if len(current_returning_customers) == 0:  # Returning customer
                continue
            random_customer = randint(0, len(current_returning_customers)-1)
            customer = current_returning_customers[random_customer]

        else:                            # Onetime customer
            if uniform(0, 1) <= prob_tripadvisor:  # tripadvisor customer
                customer = c.TripAdvisorCustomer("CID" + str(index + len(returning_customers) + 1),
                                                 consumption, budget_tripadvisor)
            else:                                  # regular (Onetime) customer
                customer = c.OneTimeCustomer("CID" + str(index + len(returning_customers) + 1), consumption,
                                             budget_onetime)

        order = customer.order(str(time[11:-3]), drinks_prices, foods_prices)

        dict = {"TIME": str(time),
                "CUSTOMER": customer.customerID,
                "DRINKS": order[0],
                "FOOD": order[1],
                "PAYROLL AMOUNT": order[2]}

        row_list.append(dict)

        # Remove the (Returning) customer if he cannot come back
        if isinstance(customer, c.ReturningCustomer) and not (customer.can_return(drinks_prices, foods_prices)):
            current_returning_customers.pop(random_customer)

    # Put the data into a DataFrame
    return pd.DataFrame(row_list, columns=["TIME", "CUSTOMER", "DRINKS", "FOOD", "PAYROLL AMOUNT"])
