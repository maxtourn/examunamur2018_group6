from Code.Part1.Graph import graphs, show_graph
from Code.Part1.Probabilities import probabilities
from Code.Part3.Simulation import simulation, create_returning
import pandas as pd
from numpy import nan


data = pd.read_csv("../../Data/Coffeebar_2013-2017.csv", sep=";")
drinks_prices = {"milkshake": 5, "frappucino": 4, "water": 2, "coffee": 3, "soda": 3, "tea": 3}
foods_prices = {"sandwich": 5, "cookie": 2, "pie": 3, "muffin": 3, nan: 0}
consumption = probabilities(data)

# Simulation
returning_customers = create_returning(consumption)
simulated_data = simulation(data["TIME"], consumption, drinks_prices, foods_prices, returning_customers)
simulated_data.to_csv("../../Data/SimulatedData.csv", sep=";", index=False)  # Save the data in a csv file

# ----------------------------------------------------------------------------------------------------------------------

# Graphs
graphs(simulated_data)

# ----------------------------------------------------------------------------------------------------------------------

# Some additional graphs

# The average income during the day:

data2 = data.copy()

data2["TIME"] = data["TIME"].str[11:13]
data2["PAYROLL AMOUNT"] = data2["DRINKS"].map(lambda x: drinks_prices[x]) + data2["FOOD"].map(lambda x: foods_prices[x])

nbr_of_days = data["TIME"].str[0:10].describe()["unique"]
simulated_data["TIME"] = simulated_data["TIME"].str[11:13]

df_income_day = simulated_data.groupby(['TIME'])[["PAYROLL AMOUNT"]].sum() / nbr_of_days
df_income_day = pd.concat([data2.groupby(['TIME'])[["PAYROLL AMOUNT"]].sum() / nbr_of_days, df_income_day], axis=1)
df_income_day.columns = ['Original', 'Simulation']

show_graph(df_income_day, 'Average income per day by hour')

# The average income per order and per hours:
df_income_order = simulated_data[["TIME", "PAYROLL AMOUNT"]].groupby("TIME").mean()
df_income_order = pd.concat([data2[["TIME", "PAYROLL AMOUNT"]].groupby("TIME").mean(), df_income_order], axis=1)
df_income_order.columns = ['Original', 'Simulation']

show_graph(df_income_order, 'The average income per order by hour')

# Average difference income per order
df_difference = df_income_order["Simulation"] - df_income_order["Original"]
print("Average income difference per order between simulated_data and data: ", round(df_difference.mean(), 2))
print("Average tip per order: ", round(0.8 * 0.1 * (10 + 1)/2, 2))
