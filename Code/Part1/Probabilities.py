def probabilities(data):
    """Gives probabilities to buy certain foods and drinks at any moment"""

    data2 = data.copy()
    data2["TIME"] = data["TIME"].str[11:-3]  # Keeps the hours and the minutes (ex: 2013-01-01 13:24:00 => 13:24)
    nbr_of_days = data["TIME"].str[0:10].describe()["unique"]  # Number of different days in the data

    consumption = dict()

    # Determine the probabilities at any time
    for line in data2.values:
        # line[0] : TIME, line[1] : CUSTOMER, line[2] : DRINKS, line[3] : FOOD
        consumption[line[0]] = consumption.get(line[0], {"DRINKS": {line[2]: 0}, "FOOD": {line[3]: 0}})
        consumption[line[0]]["DRINKS"][line[2]] = consumption[line[0]]["DRINKS"].get(line[2], 0) + 1/nbr_of_days
        consumption[line[0]]["FOOD"][line[3]] = consumption[line[0]]["FOOD"].get(line[3], 0) + 1/nbr_of_days

    return consumption


def display_probabilities(consumption):
    """Display the results obtained by the function above (probabilities)"""
    for hours in consumption:
        drinks = ""
        foods = ""
        for drinks_foods in consumption[hours]:
            for key, value in consumption[hours][drinks_foods].items():
                if drinks_foods == "DRINKS":
                    drinks += str(key) + " is " + str((int(value*10000)) / 100) + "%, "
                    #                               |_> Transform in % en keep 2 significant digits
                else:
                    foods += str(key) + " is " + str((int(value*10000)) / 100) + "%, "   # same

        print("On average the probability of a customer at %s buying %s\nand for food, %s" % (hours, drinks, foods))
