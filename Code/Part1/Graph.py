import matplotlib.pylab as plt


def show_graph(data, title, rounding=2, kind='bar', annotation=True):
    """Takes a DataFrame to create a graph and display it with a title. This graph can be annotated by its values"""
    plot = data.round(rounding).plot(kind=kind, title=title)
    if annotation:  # Annotates the graph
        for p in plot.patches:  # Show the value of the bars
            plot.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005))
            #                                           |_> where to place the value <_|
    plt.show(block=False)


def graphs(data):
    """Display some graphs from a DataFrame"""

    count_drinks = data["DRINKS"].value_counts().sort_index()  # Count how many times each drink was purchased
    count_food = data["FOOD"].value_counts().sort_index()      # Count how many times each food was purchased

    # Total amount of food
    show_graph(count_food, 'Total amount of sold food')

    # Total amount of drinks
    show_graph(count_drinks, 'Total amount of sold drinks')

    # Hourly breakdown
    n_days = data["TIME"].str[0:10].describe()["unique"]  # Number of different days in the data

    data2 = data.copy()  # in order not to modify data
    data2["TIME"] = data["TIME"].str[11:13]  # Keeps the hours (ex: 2013-01-01 08:00:00 => 08)

    consumption_per_hour = data2[["TIME", "DRINKS", "FOOD"]].groupby("TIME").count()/n_days
    show_graph(consumption_per_hour, 'Hourly breakdown of the total drink and food consumption', annotation=False)

    # Breakdown by TIME and by DRINK
    drink_per_hour = data2.groupby(['TIME', 'DRINKS']).size().map(lambda x: x/n_days).unstack()
    show_graph(drink_per_hour, 'Hourly breakdown by drink', annotation=False)

    # Breakdown by TIME and by FOOD
    food_per_hour = data2.groupby(['TIME', 'FOOD']).size().map(lambda x: x/n_days).unstack()
    show_graph(food_per_hour, 'Hourly breakdown by food', annotation=False)
