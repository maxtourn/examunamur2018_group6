import pandas as pd
from Code.Part1.Graph import graphs
from Code.Part1.Probabilities import probabilities, display_probabilities


data = pd.read_csv("../../Data/Coffeebar_2013-2017.csv", sep=";")

# What food and drinks are sold by the coffee bar?

# Drinks
drinks = ""
for drink in data["DRINKS"].unique(): drinks += drink + ", "  # transform the list into a string (for a better display)
print("The drinks sold by the bar are:", drinks)

# Food
foods = ""
for food in data["FOOD"].dropna(axis=0).unique(): foods += food + ", "  # same but drops lines with "nan" value
print("The food sold by the bar are:", foods)

# ----------------------------------------------------------------------------------------------------------------------

# How many unique customers did the bar have?
print("There are %s unique customers" % data["CUSTOMER"].describe()["unique"])

# ----------------------------------------------------------------------------------------------------------------------

# Make (AT LEAST THESE PLOTS!) : a bar plot of the total amount of sold foods (plot1) and drinks (plot2) over the five
# years
graphs(data)

# ----------------------------------------------------------------------------------------------------------------------

# Determine the average that a customer buys a certain food or drink at any given time
consumption = probabilities(data)

# Display consumption
display_probabilities(consumption)
